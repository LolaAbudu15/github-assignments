public class HomeInsurance implements Detailable {
    private double premium;
    private double excess;
    private double amount;

    public HomeInsurance(double premium, double excess, double amount) {
        this.premium = premium;
        this.excess = excess;
        this.amount = amount;
    }

    @Override
    public String getDetails() {
        return "The premium is " + premium + "\n The excess is " + excess + "\n The amount is " + amount;
    }
}