public class TestAccount2 {
    public static void main(String[] args) {
        //below is chp 8 - adding constructors - might be wrong
        //creating an array of 5 Accounts
        Account[] arrayOfAccounts2 = new Account[5];
        double[] amounts2 = {230,54440,20,3450,340};
        String[] names2 = {"Lola", "Wolf", "Troy", "Data","Ryker"};

        for (int i = 0; i < arrayOfAccounts2.length; i++) {
            //arrayOfAccounts2[i] = new Account();
            arrayOfAccounts2[i].setName(names2[i]);
            arrayOfAccounts2[i].setBalance(amounts2[i]);

            System.out.println(arrayOfAccounts2[i].getName() + 
                "\t Balance = " + arrayOfAccounts2[i].getBalance());

            arrayOfAccounts2[i].addInterest();
            System.out.println(arrayOfAccounts2[i].getName() + 
            "\t Balance plus Interest = " + arrayOfAccounts2[i].getBalance()+ '\n');

            //arrayOfAccounts2[i].setInterestRate(5.5);
        }
        
        //arrayOfAccounts2.
    }
}