public class Car {
  
    //default constructor -  must put this back if you put the below constructor if you want to be able to use different params
    //public Car(){} 
   
    //constructor - remb when you add a constructor the default constructor is gone
    public Car(String mk, String md){
        make = mk;
        model = md;
    }
   
    //properties
    private String make; 
     //private classes are only accesible within the class, we provide methods to retrieve it
    private String model;

    //getters/ setters
    public String getMake(){return make;}
    public String getModel(){return model;}

    public void setMake(String make){
        this.make = make;
    };
    public void setModel(String model){
        this.model = model;
    };
}