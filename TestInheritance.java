public class TestInheritance {
    public static void main(String[] args) {

        Account[] account = {

            new SavingsAccount("Lola", 6),
            new SavingsAccount("Maria", 4),
            new CurrentAccount("Smith", 2)
        };

        for (int i = 0; i < account.length; i++) {
            account[i].addInterest();
            System.out.println("Name = " + account[i].getName());
            System.out.println("Balance = " + account[i].getBalance() + '\n');
        }
    }
}