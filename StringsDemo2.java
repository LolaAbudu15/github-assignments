public class StringsDemo2 {
    public static void main(String[] args) {
        String name1 = "hello";
        String name2 = new String("hello");

        // what will the result of this comparison be - different because you have now created a brand new 
        // object reference in memory (not using the same string pool)
        if (name1 == name2){
            System.out.println("They are the same");
        } else 
            System.out.println("They are different");
    }
}