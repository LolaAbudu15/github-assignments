public class TestInterfaces {
    public static void main(String[] args) {
        Detailable[] myDetailables = {
            new CurrentAccount("Lola", 500),
            new HomeInsurance(2, 3, 4),
            new SavingsAccount("Maria", 50)          
        };

        for (int i = 0; i < myDetailables.length; i++) {
            System.out.println('\n' + myDetailables[i].getDetails() + '\n');
        }
    }
}