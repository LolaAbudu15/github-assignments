public abstract class Account implements Detailable {
    //instance variables
    private String name;
    private double balance;
    //static variable
    private static double interestRate = 0.35;
    
    //no argument constructor that takes a String and number param
    //?ask chp 8 question 2 
    public Account (){
        this("Lola", 50);
    }
    //constructor
    public Account(String name, double balance){
        this.name = name;
        this.balance = balance;
    }
    
    public boolean withdraw(double amount){
        boolean withdrwalResult = false;
        if(balance > amount){
            System.out.println("Balance is high enough for withdrawal of $" + amount);
            balance = balance - amount;
            withdrwalResult = true;
            System.out.println("Post withdrawal balance = $" + balance);            
        } else 
            System.out.println("Balance is too low");       
            System.out.println("You only have $" + balance + " in your account");
        return withdrwalResult;
    }    

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public abstract void addInterest();
    // {
    //  //   this.balance = this.balance + (this.balance * interestRate);    //emptying this content
    // }

    public static double getInterestRate() {
        return interestRate;
    }

    public static void setInterestRate(double interestRate) {
        Account.interestRate = interestRate;
    }

    public String getDetails(){
        return "Name is " + name + "\n Balance is " + balance;
    }

    @Override
    public String toString() {
        return "Account [balance=" + balance + ", name=" + name + "]";
    }
}