import java.util.HashSet;

public class CollectionsTest {
    public static void main(String[] args) {
        HashSet<Account> allAccounts = new HashSet<Account>();

        double[] amounts ={2,3,4,5,6};
        String[] names = {"Lola", "Wolf", "Troy", "Maria","Ryker"};

        Account[] eachAccounts;
        for (int i = 0; i < 5; i++) {
           eachAccounts = new Account[5];
           eachAccounts[i].setName(names[i]); 
           eachAccounts[i].setBalance(amounts[i]);
           allAccounts.add(eachAccounts[i]);
           System.out.println("All Account " + allAccounts);
           System.out.println("Each Account " + eachAccounts);
            //? seems i have  NullPointerException
        }

        for (Account secondAccount : allAccounts) {
            System.out.println("Name " + secondAccount.getName());
            System.out.println("Balance " + secondAccount.getBalance());
        }
    }
}