public class StingsDemo1 {
    public static void main(String[] args) {
        String name1 = "hello";
        String name2 = "hello";

        // what will the result of this comparison be - the same because we are comparing object 
        // reference in memory and not the string value itself due to string pool that was created
        // in java to help with memory storage (both above strings are stored in the same string 
        // pool- use .equal() to compare the 2 string values)
        if (name1 == name2){
            System.out.println("They are the same");
        } else 
            System.out.println("They are different");
    }
}